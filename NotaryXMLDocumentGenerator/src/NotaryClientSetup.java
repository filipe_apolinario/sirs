import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;

import core.FileManager;
import core.XmlDocumentGenerator;
import core.security.KeyManager;


public class NotaryClientSetup {
    private static final int KEYSIZE = 3072;

    public static void main(String[] args) {
        HashMap<String, KeyPair> keyPairByUser = generateUserKeys();
        Collection<String> users = keyPairByUser.keySet();
        String xmlDocument = XmlDocumentGenerator.generateXMLDocument(
                "eu florisberta devo 100 euros a esdrubalina", users, keyPairByUser);
        System.out.println(xmlDocument);
        try {
            FileManager.writeFile("xml/", "document1.xml", xmlDocument.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static HashMap<String, KeyPair> generateUserKeys() {
        HashMap<String, KeyPair> keyPairByUser = new HashMap<String, KeyPair>();
        try {
            String userName = null;
            KeyPair k = null;
            System.out.println("Generating RSA keys ...");
            userName = "florisberta";
            k = generateUserRSAKey(userName);
            keyPairByUser.put(userName, k);

            userName = "esdrubalina";
            k = generateUserRSAKey(userName);
            keyPairByUser.put(userName, k);
            System.out.println("Successfuly saved users' RSA Keys");
            String document = readFile("documentsign.xml", Charset.defaultCharset());
            return keyPairByUser;

        } catch (Exception e) {
            System.out.println("Não foi possivel gerar chaves do servidor");
            System.out.println(e.getMessage());
        }
        return keyPairByUser;
    }

    private static KeyPair generateUserRSAKey(String name) throws NoSuchAlgorithmException,
            InvalidAlgorithmParameterException,
            Exception {
        KeyPair keyPair = KeyManager.generateRSAKeyPair(KEYSIZE);
        KeyManager.writeNotaryRSAKeyPair(keyPair, name);
        return keyPair;
    }

    private static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }






}
