package core;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import core.security.SignatureHandler;
import core.security.ws.MessageParser;

public class XmlDocumentGenerator {

    public static String generateXMLDocument(String text,
                                             Collection<String> participants,
                                             HashMap<String, KeyPair> participantsKeyPair) {

        //Create and Initialize XML Document
        Document doc = createNewDocumentXML();
        Element rootElement = createAndApendElement("doc", doc, doc);

        //Create document with text and participants
        Element documentElement = addDocumentToXML(text, participants, doc, rootElement);

        //sign document for each participant and apend signatures to xml 
        String documentElementText = TextExtractor.extractText(documentElement);
        addSignaturesToXML(participants, participantsKeyPair, doc, rootElement, documentElementText);

        return TextExtractor.extractText(rootElement);

    }

    private static Element createAndApendElement(String tagName, Document doc, Document doc2) {
        Element element = doc.createElement(tagName);
        doc2.appendChild(element);
        return element;
    }

    private static Element addSignaturesToXML(Collection<String> participants,
                                              HashMap<String, KeyPair> participantsKeyPair,
                                              Document doc,
                                              Element rootElement,
                                              String documentElementText) {

        Element signaturesElement = createAndApendElement("signatures", doc, rootElement);
        for (String p : participants) {
            Element signatureElement = createAndApendElement("signature", doc, signaturesElement);

            Element nameElement = createAndApendTextAtribute("name", p, doc, signatureElement);

            KeyPair k = participantsKeyPair.get(p);
            String pubKeyBase64 = MessageParser.toXML(k.getPublic().getEncoded());
            Element pubKeyElement = createAndApendTextAtribute("key", pubKeyBase64, doc,
                    signatureElement);

            PrivateKey privKey = k.getPrivate();
            byte[] digest = null;
            try {
                digest = SignatureHandler.makeDigitalSignature(documentElementText.getBytes(),
                        privKey);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Element digestElement = createAndApendTextAtribute("digest", MessageParser
                    .toXML(digest).toString(), doc, signatureElement);
        }
        return signaturesElement;
    }

    private static Element addDocumentToXML(String text,
                                            Collection<String> participants,
                                            Document doc,
                                            Element rootElement) {
        Element documentElement = createAndApendElement("document", doc, rootElement);
        Element textElement = createAndApendTextAtribute("text", text, doc, documentElement);
        addParticipants(participants, doc, documentElement);

        return documentElement;
    }

    private static void addParticipants(Collection<String> participants,
                                        Document doc,
                                        Element rootElement) {
        Element participantsElement = createAndApendElement("participants", doc, rootElement);
        for (String p : participants) {
            createAndApendTextAtribute("name", p, doc, participantsElement);
        }
    }

    private static Element createAndApendElement(String tagName, Document doc, Element rootElement) {
        Element element = doc.createElement(tagName);
        rootElement.appendChild(element);
        return element;
    }

    private static Element createAndApendTextAtribute(String tagName,
                                                      String value,
                                                      Document doc,
                                                      Element rootElement) {
        Element element = doc.createElement(tagName);
        element.appendChild(doc.createTextNode(value));
        rootElement.appendChild(element);
        return element;
    }

    private static Document createNewDocumentXML() {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Document doc = docBuilder.newDocument();
        return doc;
    }
}
