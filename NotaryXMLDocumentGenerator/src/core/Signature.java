package core;

import java.security.PublicKey;

public class Signature {

    private byte[] digest;
    private PublicKey key;

    public Signature(byte[] digest, PublicKey key) {
        this.digest = digest;
        this.key = key;
    }
}
