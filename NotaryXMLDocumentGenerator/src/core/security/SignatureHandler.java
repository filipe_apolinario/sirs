package core.security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

public class SignatureHandler {


    /**
     * Verify digital signature.
     * 
     * @param cipheredDigest the cipher digest
     * @param bytes the bytes
     * @return true, if successful
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws Exception the exception
     */
    static public boolean verifyDigitalSignature(byte[] bytes, byte[] cipheredDigest, PublicKey key) throws NoSuchAlgorithmException,
            InvalidKeyException,
            SignatureException {

        // verify the signature with the public key
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initVerify(key);
        sig.update(bytes);
        try {
            return sig.verify(cipheredDigest);
        } catch (SignatureException se) {
            return false;
        }
    }

    /**
     * Make digital signature.
     * 
     * @param bytes the bytes
     * @return the byte[]
     * @throws Exception the exception
     */
    static public byte[] makeDigitalSignature(byte[] bytes, PrivateKey key) throws Exception {

        Signature sig = Signature.getInstance("SHA256withRSA");

        sig.initSign(key);
        sig.update(bytes);
        byte[] signature = sig.sign();

        return signature;
    }

}
