package core.security.ws;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import core.security.KeyManager;
import core.security.SignatureHandler;

public class DocumentHandler {

    public DocumentHandler() {
        // TODO Auto-generated constructor stub
    }

    public static boolean isDocumentValid(String documentXML, HashMap<String, PublicKey> keys) throws InvalidKeyException,
            NoSuchAlgorithmException,
            SignatureException {

        Document document;
        try {
            document = xmlStringToDocument(documentXML);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            return false;
        }
        Element rootElement = document.getDocumentElement();
        if (rootElement == null) {
            return false;
        }
        System.out.println(rootElement.getNodeName());
        NodeList nodes = rootElement.getChildNodes();
        if (nodes == null) {
            return false;
        }
        System.out.println(nodes.toString());
        HashMap<String, String> digitalSignatures = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node instanceof Element) {
                //a child element to process
                Element child = (Element) node;
                //System.out.println("estou no node " + child.getNodeName() + "\n\n\n");
                if (child == null) {
                    return false;
                }
                if (child.getNodeName().equals("signatures")) {
                    digitalSignatures = getDocumentSignatures(child);
                } else if (child.getNodeName().equals("document")) {
                    Collection<String> participants = getParticipants(child);
                    for (String participant : participants) {
                        if (!hasParticipantSignedText(digitalSignatures, child, participant)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private static Node getNodeAtribute(Node node, String atribute) {
        return node.getAttributes().getNamedItem(atribute);
    }

    private static boolean hasParticipantSignedText(HashMap<String, String> digitalSignatures,
                                                    Node document,
                                                    String participant) throws NoSuchAlgorithmException,
            InvalidKeyException,
            SignatureException {
        String dig = digitalSignatures.get(participant);

        if (dig == null) {
            //System.out.println("quem estragou o esquema foi" + participant);
            return false;
        }
        //PublicKey participantKey = keys.get(participant);
        //if (participantKey == null) {
        //    return false;
        //}
        System.out.println();
        try {
            KeyManager.writeFile(
                    "usrs" + "/" + participant + "/signature/",
                    "digest.txt",
                    MessageParser.toXML(
                            SignatureHandler.makeDigitalSignature(
                                    MessageParser.fromXML(document.toString()),
                                    KeyManager.readNotaryRSAPrivateKey(participant))).getBytes());
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    private static Document xmlStringToDocument(String documentXML) throws SAXException,
            IOException,
            ParserConfigurationException {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        builder = builderFactory.newDocumentBuilder();

        System.out.println("o documento xml:\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        document = builder.parse(new InputSource(new ByteArrayInputStream(documentXML
                .getBytes("utf-8"))));

        return document;
    }

    private static Collection<String> getParticipants(Node namedItem) throws RuntimeException {
        // TODO Auto-generated method stub
        NodeList nodeList = namedItem.getChildNodes();
        Collection<String> participants = new ArrayList<String>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodeChild = nodeList.item(i);
            if (nodeChild instanceof Element) {
                if (nodeChild == null) {
                    throw new RuntimeException();
                }
                System.out.println("estou na " + nodeChild.getNodeName());
                NodeList nlst = nodeChild.getChildNodes();
                for (int j = 0; j < nlst.getLength(); j++) {

                    Node n = nlst.item(j);
                    if (n == null) {
                        throw new RuntimeException();
                    }
                    System.out.println("estou na " + n.getNodeName());
                    if (nodeChild instanceof Element) {
                        if (n.getNodeName().equals("name")) {
                            System.out.println(n.getTextContent());
                            participants.add(n.getTextContent());

                        }
                    }

                }
            }
        }
        return participants;
    }

    private static HashMap<String, String> getDocumentSignatures(Node node) {
        NodeList nodeList = node.getChildNodes();
        HashMap<String, String> participantsSignatures = new HashMap<String, String>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            //We have encountered an <employee> tag.
            Node nodeChild = nodeList.item(i);
            if (nodeChild instanceof Element) {
                System.out.println("estou na " + nodeChild.getNodeName());
                NodeList nlst = nodeChild.getChildNodes();
                if (nlst == null) {
                    throw new RuntimeException();
                }
                String participantName = null;
                String participantDigest = null;
                for (int j = 0; j < nlst.getLength(); j++) {
                    Node n = nlst.item(j);
                    if (n == null) {
                        throw new RuntimeException();
                    }
                    if (nodeChild instanceof Element) {
                        if (n.getNodeName().equals("name")) {
                            participantName = n.getTextContent();
                        }
                        if (n.getNodeName().equals("digest")) {
                            participantDigest = n.getTextContent();
                        }
                        System.out.println("estou na " + n.getNodeName());
                        System.out.println(n.getNodeName() + "tem o seguinte text"
                                + n.getTextContent());
                    }

                }
                participantsSignatures.put(participantName, participantDigest);
            }
        }
        return participantsSignatures;
    }
}
