package core.security;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import core.FileManager;

public class KeyManager {

    private static String path = "chave/";
    private static String privateKey = "Public.key";
    private static String publicKey = "Private.key";



    public static void writeNotaryRSAKeyPair(KeyPair keyPair, String name) throws Exception {
        //TODO Remove System.out
        // generate RSA key pair


        System.out.println("Public key info:");
        System.out.println("algorithm: " + keyPair.getPublic().getAlgorithm());
        System.out.println("format: " + keyPair.getPublic().getFormat());

        System.out.println("---");

        System.out.println("Private key info:");
        System.out.println("algorithm: " + keyPair.getPrivate().getAlgorithm());
        System.out.println("format: " + keyPair.getPrivate().getFormat());

        System.out.println("Writing private key to '" + privateKey + "' ...");
        byte[] privEncoded = keyPair.getPrivate().getEncoded();
        FileManager.writeFile("usrs/" + name + "/" + path, privateKey, privEncoded);

        System.out.println("Writing public key to " + publicKey + " ...");
        byte[] pubEncoded = keyPair.getPublic().getEncoded();
        FileManager.writeFile("usrs" + "/" + name + "/" + path, publicKey, pubEncoded);


    }

    public static KeyPair generateRSAKeyPair(int keySize) throws NoSuchAlgorithmException,
            InvalidAlgorithmParameterException {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keySize);
        KeyPair key = keyGen.generateKeyPair();
        return key;
    }



    public static PrivateKey readNotaryRSAPrivateKey(String name) throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            FileNotFoundException,
            IOException {
        byte[] privEncoded = FileManager.readFile("usrs/" + name + "/" + path + privateKey);
        PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privEncoded);
        KeyFactory keyFacPriv = KeyFactory.getInstance("RSA");
        final PrivateKey key = keyFacPriv.generatePrivate(privSpec);
        return key;
    }

    public static PublicKey readNotaryRSAPublicKey(String name) throws FileNotFoundException,
            IOException,
            NoSuchAlgorithmException,
            InvalidKeySpecException {
        byte[] pubEncoded = FileManager.readFile("usrsr/" + name + "/" + path + publicKey);
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pubEncoded);
        KeyFactory keyFacPub = KeyFactory.getInstance("RSA");
        final PublicKey key = keyFacPub.generatePublic(pubSpec);
        return key;
    }

}
