package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

// TODO: Auto-generated Javadoc
/**
 * The Class Cypher. This class is responsible for the signing a document with the TSA key
 */
public class Cypher {

    /**
     * The private key from the TSA.
     */
    private PrivateKey key;

    {
        try {
            byte[] privEncoded = readFile("chave/Private.key");
            PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privEncoded);
            KeyFactory keyFacPriv = KeyFactory.getInstance("RSA");
            key = keyFacPriv.generatePrivate(privSpec);
            //System.out.println(key.serialVersionUID);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            System.out.println("problema na geração da chave");
            e.printStackTrace();
        }
    }

    /**
     * Read file.
     * 
     * @param path the path
     * @return the byte[]
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private byte[] readFile(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] content = new byte[fis.available()];
        fis.read(content);
        fis.close();
        return content;
    }

    /**
     * Make digital signature.
     * 
     * @param bytes the bytes
     * @return the byte[]
     * @throws Exception the exception
     */
    public byte[] makeDigitalSignature(byte[] bytes) throws Exception {

        Signature sig = Signature.getInstance("SHA256withRSA");

        sig.initSign(key);
        sig.update(bytes);
        byte[] signature = sig.sign();

        return signature;
    }

}
