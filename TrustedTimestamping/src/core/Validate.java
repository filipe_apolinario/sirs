package core;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

import java.sql.Timestamp;

public class Validate {

    public String validateSignature(String signature) throws Throwable {
        //TODO gerar chave
        Timestamper t = new Timestamper();
        Timestamp timestmp = t.getTimestamp();

        String signatureWithTimestamp = signature + timestmp;

        Cypher c = new Cypher();

        byte[] validatedSignature = c
                .makeDigitalSignature(parseBase64Binary(signatureWithTimestamp));

        return printBase64Binary(validatedSignature) + timestmp;

    }





}
