package core;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;

public class HistoryLogger {

    // TODO Auto-generated constructor stub
    private String historyLogPath;

    {
        historyLogPath = "TTHistory.log";
    }

    public void addToLog(LogEntry entry) throws IOException, ClassNotFoundException {
        Collection<LogEntry> historyLog = readObject();
        if (historyLog != null) {
            historyLog.add(entry);
        } else {
            historyLog = new ArrayList<LogEntry>();
            historyLog.add(entry);
        }
        writeObject(historyLog);
    }

    public Collection<LogEntry> getHistoryLog() throws FileNotFoundException,
            IOException,
            ClassNotFoundException {
        return readObject();
    }


    public void writeObject(Collection<LogEntry> historyLog) throws IOException {
        FileOutputStream fos = new FileOutputStream(historyLogPath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(historyLog);
        oos.close();
    }

    public Collection<LogEntry> readObject() throws IOException, ClassNotFoundException {
        try {
            FileInputStream fileInput = new FileInputStream(historyLogPath);

            ObjectInputStream inputStream = new ObjectInputStream(fileInput);
            return (Collection<LogEntry>) inputStream.readObject();
        } catch (EOFException | FileNotFoundException e) {
            return null;
        }

    }

    public String toString() {
        Collection<LogEntry> log;
        try {
            log = getHistoryLog();
            if (log != null) {
                String logString = "";
                for (LogEntry entry : log) {
                    logString += entry + "\n";
                }
                return logString;
            }
            return null;
        } catch (ClassNotFoundException | IOException e) {
            return null;
        }

    }
}
