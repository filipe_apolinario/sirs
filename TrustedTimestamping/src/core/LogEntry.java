package core;

import java.io.Serializable;

public class LogEntry
        implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6981609128974959722L;
    /**
     * 
     */
    private final String timestamp;
    private final String tSASignature;
    private final String originalSignature;


    public LogEntry(String timestamp, String tSASignature, String originalSignature) {
        this.timestamp = timestamp;
        this.tSASignature = tSASignature;
        this.originalSignature = originalSignature;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @return the tSASignature
     */
    public String getTSASignature() {
        return tSASignature;
    }

    public String getOriginalSignature() {
        return originalSignature;
    }

    public String toString() {
        return "[" + this.getTimestamp() + "] " + "assinada a assinatura \'"
                + this.getOriginalSignature() + "\' resultando na assinatura \'"
                + this.getTSASignature() + "\' tendo sido enviada ao cliente";
    }




}
