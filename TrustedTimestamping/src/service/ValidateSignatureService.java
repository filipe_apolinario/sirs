package service;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

import java.sql.Timestamp;

import service.dto.ValidatedDocumentDto;
import core.Cypher;
import core.HistoryLogger;
import core.LogEntry;
import core.Timestamper;

public class ValidateSignatureService {

    private final String inputSignature;

    private ValidatedDocumentDto result;

    public ValidateSignatureService(String signature) {
        inputSignature = signature;
    }

    public void execute() throws Throwable {

        Timestamp timestamp = Timestamper.getTimestamp();
        String signatureWithTimestamp = inputSignature + timestamp;

        Cypher c = new Cypher();

        byte[] tsaSignature = c.makeDigitalSignature(parseBase64Binary(signatureWithTimestamp));

        result = new ValidatedDocumentDto(timestamp.toString(), printBase64Binary(tsaSignature));

        HistoryLogger h = new HistoryLogger();
        LogEntry newLogEntry = new LogEntry(result.getTimestamp(), result.getTSASignature(),
                inputSignature);
        h.addToLog(newLogEntry);

    }

    public ValidatedDocumentDto getResult() {
        return result;
    }
}
