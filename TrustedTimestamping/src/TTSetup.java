import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

public class TTSetup {
    private static final int KEYSIZE = 3072;

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        String publicKeyPath = "chave/" + "Public.key";
        String privateKeyPath = "chave/" + "Private.key";
        try {
            write(publicKeyPath, privateKeyPath);
        } catch (Exception e) {
            System.out.println("Não foi possivel gerar chaves do servidor");
            System.out.println(e.getMessage());
        }
    }



    private static void writeFile(String path, byte[] content) throws FileNotFoundException,
            IOException {
        FileOutputStream fos = new FileOutputStream(path);
        fos.write(content);
        fos.close();
    }

    public static void write(String publicKeyPath, String privateKeyPath) throws Exception {

        // generate RSA key pair
        System.out.println("Generating RSA keys ...");
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(KEYSIZE);
        KeyPair key = keyGen.generateKeyPair();

        System.out.println("Public key info:");
        System.out.println("algorithm: " + key.getPublic().getAlgorithm());
        System.out.println("format: " + key.getPublic().getFormat());

        System.out.println("Writing public key to " + publicKeyPath + " ...");
        byte[] pubEncoded = key.getPublic().getEncoded();
        writeFile(publicKeyPath, pubEncoded);

        System.out.println("---");

        System.out.println("Private key info:");
        System.out.println("algorithm: " + key.getPrivate().getAlgorithm());
        System.out.println("format: " + key.getPrivate().getFormat());

        System.out.println("Writing private key to '" + privateKeyPath + "' ...");
        byte[] privEncoded = key.getPrivate().getEncoded();
        writeFile(privateKeyPath, privEncoded);
    }

}
