package presentation.cli;

import service.ValidateSignatureService;
import exception.BadConnectionException;
import exception.InvalidSignatureValidationException;

// classes generated from WSDL


public class ClientMain {

    protected static final String DOCUMENT = "VuNJ1KHclq2D39BOhUI6BcbC/ENUs2OwfA/r5L0DDWd85TI3Prr5CKfwQmw4TduM4YCbxbLwAu5VUgRMfRTr0Ya1pAxg62Dtz8DUHsq3aK8X+7mUomVJ32Sgz3svHD+CUxZ4ITT5WsH4HD7Zm0OpWwPW/wkKWLTfvXc+Dr1ZrQ2GKDxAxOxRwk8gcOalosgeSm9PnTTehPkmKTKkSgJ1nflC/JjXwpfVZYtItyXq9x9Bi/YcxF4cTPD5kTKB7RuHEjnCHUdJGycXYsGR9TDDYcHzjhktYkqfm5mGIvEE3p7XM2afUF22zWx6761QdIoOX728ipN1SqH3dP3d3tVTqpySUmaXzktfo5xZ1JhkA4zx71co22Y5PlDfRrFoOIZYP8XbLlzZiCynO9WbtWF9nYMrZpRFHAYih4p5hCthuPM47h2ArihHDRRfCSNSXFZ3TD6XbDTqJm3sl6Gv8YROguJDx2AbSiwiIthOvHnVwoIsSBBXZ/AstyufImyBkJkL";

    public static void main(String[] args) {

        ValidateSignatureService s = new ValidateSignatureService(DOCUMENT);
        try {
            s.execute();
            System.out.println("signature and timestamps are working");
        } catch (BadConnectionException e) {
            e.printStackTrace();
        } catch (InvalidSignatureValidationException e) {
            e.printStackTrace();
        }
    }
}
