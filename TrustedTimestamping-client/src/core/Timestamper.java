package core;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class Timestamper {

    private static Calendar c = Calendar.getInstance();

    public static Timestamp getTimestamp() {

        Date date = c.getTime();
        long time = date.getTime();

        return new Timestamp(time);
    }

}
