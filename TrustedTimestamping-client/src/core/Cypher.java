package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;



// TODO: Auto-generated Javadoc
/**
 * The Class Cypher. This class is responsable for verifying the signature
 * 
 * @author Filipe Apolinário
 */
public class Cypher {

    /**
     * The public key from TSA.
     */
    private static PublicKey key;


    /**
     * static initializer - initializes the publicKey.
     */
    {
        try {
            byte[] pubEncoded = readFile("chave/TSAPublic.key");
            X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pubEncoded);
            KeyFactory keyFacPub = KeyFactory.getInstance("RSA");
            key = keyFacPub.generatePublic(pubSpec);
            System.out.println("chave");
            System.out.println(key.serialVersionUID);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("nao encontrada chave");
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            System.out.println("problema na geração da chave");
            e.printStackTrace();
        }
    }

    /**
     * Read file.
     * 
     * @param path the path
     * @return the byte[]
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private byte[] readFile(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] content = new byte[fis.available()];
        fis.read(content);
        fis.close();
        return content;
    }

    /**
     * Verify digital signature.
     * 
     * @param cipherDigest the cipher digest
     * @param bytes the bytes
     * @return true, if successful
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws Exception the exception
     */
    public boolean verifyDigitalSignature(byte[] cipherDigest, byte[] bytes) throws NoSuchAlgorithmException,
            InvalidKeyException,
            SignatureException {

        // verify the signature with the public key
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initVerify(key);
        sig.update(bytes);
        try {
            return sig.verify(cipherDigest);
        } catch (SignatureException se) {
            return false;
        }
    }


}
