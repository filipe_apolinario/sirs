package service;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;
import static javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import presentation.ws.uddi.UDDINaming;
import service.dto.ValidatedDocumentDto;
import core.Cypher;
import exception.BadConnectionException;
import exception.InvalidSignatureValidationException;
import ws.*;

public class ValidateSignatureService {

    private final String signature;

    private service.dto.ValidatedDocumentDto result;

    public ValidateSignatureService(String signature) {
        this.signature = signature;
        this.result = null;
    }

    public void execute() throws BadConnectionException, InvalidSignatureValidationException {
        TrustedTimestampingImpl port = null;
        try {
            String uddiURL = "http://localhost:8081";
            String name = "TrustedTimestamping";

            System.out.printf("Contacting UDDI at %s%n", uddiURL);
            UDDINaming uddiNaming = new UDDINaming(uddiURL);

            System.out.printf("Looking for '%s'%n", name);
            String endpointAddress = uddiNaming.lookup(name);

            if (endpointAddress == null) {
                System.out.println("Not found!");
                return;
            } else {
                System.out.printf("Found %s%n", endpointAddress);
            }

            System.out.println("Creating stub ...");
            TrustedTimestampingImplService service = new TrustedTimestampingImplService();
            port = service.getTrustedTimestampingImplPort();

            System.out.println("Setting endpoint address ...");
            BindingProvider bindingProvider = (BindingProvider) port;
            Map<String, Object> requestContext = bindingProvider.getRequestContext();
            requestContext.put(ENDPOINT_ADDRESS_PROPERTY, endpointAddress);

            System.out.println("Remote call ...");
        } catch (Exception e) {
            throw new BadConnectionException("Could not connect to TSA");
        }

        ws.ValidatedDocumentDto validatedSignature = port.validateSignature(signature);

        System.out
                .println(validatedSignature.getTSASignature() + validatedSignature.getTimestamp());

        if (isSignatureValidated(validatedSignature.getTSASignature(),
                validatedSignature.getTimestamp(), signature)) {
            this.result = new ValidatedDocumentDto(validatedSignature.getTimestamp(),
                    validatedSignature.getTSASignature());
        } else {
            throw new InvalidSignatureValidationException("TSA's signature or timestamp is invalid");
        }


    }

    private boolean isSignatureValidated(String signature,
                                         String timestamp,
                                         String originalSignature) {
        Cypher c = new Cypher();
        try {
            if (c.verifyDigitalSignature(parseBase64Binary(signature),
                    parseBase64Binary(originalSignature + timestamp))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public service.dto.ValidatedDocumentDto getResult() {
        return result;
    }

}
