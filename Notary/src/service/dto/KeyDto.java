package service.dto;

import java.io.Serializable;

public class KeyDto
        implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4595589216991017191L;
    private final String key;
    private final String name;

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public KeyDto(String name, String key) {
        this.name = name;
        this.key = key;
    }

}
