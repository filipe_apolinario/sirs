package service.exception;

public class InvalidDocumentStructureException extends
        NotaryException {

    public InvalidDocumentStructureException() {
        // TODO Auto-generated constructor stub
    }

    public InvalidDocumentStructureException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public InvalidDocumentStructureException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public InvalidDocumentStructureException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    public InvalidDocumentStructureException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
        // TODO Auto-generated constructor stub
    }

}
