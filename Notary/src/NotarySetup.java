import java.security.KeyPair;

import core.security.KeyManager;


public class NotarySetup {
    private static final int KEYSIZE = 3072;

    public static void main(String[] args) {
        try {
            System.out.println("Generating RSA keys ...");
            KeyPair keyPair = KeyManager.generateRSAKeyPair(KEYSIZE);
            KeyManager.writeNotaryRSAKeyPair(keyPair);
            System.out.println("Successfuly saved Notary's RSA Keys");
        } catch (Exception e) {
            System.out.println("Não foi possivel gerar chaves do servidor");
            System.out.println(e.getMessage());
        }
    }







}
