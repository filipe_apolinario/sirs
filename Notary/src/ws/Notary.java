package ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import service.exception.NotaryException;

@WebService
public interface Notary {

    @WebMethod
    String signDocument(String xmlDocument) throws NotaryException;
}
