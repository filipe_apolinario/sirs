package ws;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Map;

import javax.jws.HandlerChain;
import javax.jws.WebService;

import org.w3c.dom.Document;

import service.exception.NotaryException;
import core.KeyExtractor;
import core.ParticipantExtractor;
import core.SignatureChecker;
import core.SignatureExtractor;
import core.TextExtractor;
import core.XmlDocumentLoader;
import core.XmlDocumentStructureChecker;
import core.security.KeyManager;
import core.security.SignatureHandler;
import core.security.ws.MessageParser;

@WebService(endpointInterface = "ws.NotaryImpl")
@HandlerChain(file = "/handler-chain.xml")
public class NotaryImpl
        implements Notary {

    @Override
    public String signDocument(String xmlDocument) throws NotaryException {

        //begin parse xmlDocument
        //try {
        //try {
        //if (!DocumentHandler.isDocumentValid(xmlDocument, null))
        //    return null;
        Document clientDocument = XmlDocumentLoader.loadDocument(xmlDocument);

        if (!XmlDocumentStructureChecker.checkXmlDocumentStructure(clientDocument))
            throw new NotaryException();

        List<String> documentParticipants = ParticipantExtractor
                .extractParticipants(clientDocument);
        Map<String, PublicKey> documentKeysBySigner = KeyExtractor.extractKeys(clientDocument);
        Map<String, String> documentSignaturesBySigner = SignatureExtractor
                .extractSignatures(clientDocument);
        String documentText = TextExtractor.extractText(clientDocument);

        if (!SignatureChecker.checkSignature(documentText, documentParticipants,
                documentKeysBySigner, documentSignaturesBySigner))
            throw new NotaryException();



        //} catch (InvalidKeyException e1) {
        //    // TODO Auto-generated catch block
        //    e1.printStackTrace();
        //} catch (NoSuchAlgorithmException e1) {
        //    // TODO Auto-generated catch block
        //    e1.printStackTrace();
        //} catch (SignatureException e1) {
        //    // TODO Auto-generated catch block
        //    e1.printStackTrace();
        //}

        // Document Validated
        // Time to sign

        byte[] sigBytes = null;
        try {
            sigBytes = SignatureHandler.makeDigitalSignature(
                    MessageParser.toByteArray(xmlDocument), KeyManager.readNotaryRSAPrivateKey());
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String signature = MessageParser.toXML(sigBytes);
        return signature;
    }
}
