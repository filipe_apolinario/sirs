package core;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlDocumentLoader {
    public static Document loadDocument(String plaintext) {
        Document document;
        try {
            document = xmlStringToDocument(plaintext);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            return null;
        }

        return document;
    }

    private static Document xmlStringToDocument(String documentXML) throws SAXException,
            IOException,
            ParserConfigurationException {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new ByteArrayInputStream(documentXML
                .getBytes("utf-8"))));
        // TODO? use encoding specified on input string

        return document;
    }
}
