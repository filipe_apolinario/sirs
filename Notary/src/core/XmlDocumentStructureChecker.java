package core;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlDocumentStructureChecker {
    public static boolean checkXmlDocumentStructure(Document document) {
        Element rootElement = document.getDocumentElement();
        if (rootElement == null) {
            return false;
        }

        NodeList nodes = rootElement.getChildNodes();
        if (nodes == null) {
            return false;
        }

        int numSignatures = 0;
        int numDocuments = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node == null) {
                return false;
            }
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) node;

                if (child.getTagName().equals("signatures")) {
                    if ((++numSignatures) > 1 || (!checkSignaturesStructure(child))) {
                        // there are more "signatures" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else if (child.getTagName().equals("document")) {
                    if ((++numDocuments > 1) || (!checkDocumentStructure(child))) {
                        // there are more "document" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else {
                    // some other tag is in the document
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkSignaturesStructure(Element signatures) {
        NodeList nodes = signatures.getChildNodes();
        if (nodes == null) {
            return false;
        }

        int numSignatures = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node == null) {
                return false;
            }
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) node;

                if (child.getTagName().equals("signature")) {
                    numSignatures++;
                    if ((!checkSignatureStructure(child))) {
                        // the signature is badly structured
                        return false;
                    }
                } else {
                    // some other tag is in the document
                    return false;
                }
            }
        }
        return numSignatures > 0;
    }

    private static boolean checkSignatureStructure(Element signature) {
        NodeList nodes = signature.getChildNodes();
        if (nodes == null) {
            return false;
        }

        int numNames = 0;
        int numKeys = 0;
        int numDigests = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node == null) {
                return false;
            }
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) node;

                if (child.getTagName().equals("name")) {
                    if ((++numNames) > 1 || !elementIsLeaf(child)) {
                        // there are more "name" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else if (child.getTagName().equals("key")) {
                    if ((++numDigests > 1) || !elementIsLeaf(child)) {
                        // there are more "digest" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else if (child.getTagName().equals("digest")) {
                    if ((++numKeys > 1) || !elementIsLeaf(child)) {
                        // there are more "key" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else {
                    // some other tag is in the document
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean checkDocumentStructure(Element document) {
        NodeList nodes = document.getChildNodes();
        if (nodes == null) {
            return false;
        }

        int numParticipants = 0;
        int numTexts = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node == null) {
                return false;
            }
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) node;

                if (child.getTagName().equals("participants")) {
                    numParticipants++;
                    if ((!checkParticipantsStructure(child))) {
                        // the signature is badly structured
                        return false;
                    }
                } else if (child.getTagName().equals("text")) {
                    if ((++numTexts > 1) || !elementIsLeaf(child)) {
                        // there are more "text" tags than expected
                        // or the one that exists is badly structured
                        return false;
                    }
                } else {
                    // some other tag is in the document
                    return false;
                }
            }
        }
        return numParticipants > 0;
    }

    private static boolean checkParticipantsStructure(Element participants) {
        NodeList nodes = participants.getChildNodes();
        if (nodes == null) {
            return false;
        }

        int numParticipants = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node == null) {
                return false;
            }
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) node;

                if (child.getTagName().equals("name")) {
                    numParticipants++;
                    if (!elementIsLeaf(child)) {
                        // the signature is badly structured
                        return false;
                    }
                } else {
                    // some other tag is in the document
                    return false;
                }
            }
        }
        return numParticipants > 0;
    }

    private static boolean elementIsLeaf(Element element) {
        NodeList nodes = element.getChildNodes();
        if (nodes == null) {
            return false;
        }

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() != Node.TEXT_NODE) {
                return false;
            }
        }
        return true;
    }
}
