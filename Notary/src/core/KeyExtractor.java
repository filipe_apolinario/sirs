package core;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import core.security.KeyManager;
import core.security.ws.MessageParser;

public class KeyExtractor {
    public static Map<String, PublicKey> extractKeys(Document document) {
        Map<String, PublicKey> keys = new HashMap<String, PublicKey>();

        //Extract all signature list elements
        Element rootElement = document.getDocumentElement();
        NodeList documentElements = rootElement.getElementsByTagName("signatures");
        Element signatures = (Element) documentElements.item(0);
        NodeList signaturesElements = signatures.getElementsByTagName("signature");

        //for each signature in document, extract the signer's name and PublicKey
        for (int i = 0; i < signaturesElements.getLength(); i++) {

            Element signatureElement = (Element) documentElements.item(0);
            NodeList signerNameNodeList = signatureElement.getElementsByTagName("name");
            NodeList signerKeyNodeList = signatureElement.getElementsByTagName("key");

            //extract signer's name and key 
            String signerName = signerNameNodeList.item(i).getTextContent();
            PublicKey signerKey = null;
            try {
                String keyXML = signerKeyNodeList.item(i).getTextContent();
                signerKey = extractPublicKeyfromXML(keyXML);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException e) {
                return null;
            }

            //insert signer's key in hashmap
            keys.put(signerName, signerKey);

        }
        return keys;
    }

    private static PublicKey extractPublicKeyfromXML(String keyXML) throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            IOException {
        return KeyManager.byteToPublicKey(MessageParser.fromXML(keyXML));
    }
}
