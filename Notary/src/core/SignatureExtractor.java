package core;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SignatureExtractor {
    public static Map<String, String> extractSignatures(Document document) {
        Map<String, String> extractedSignatures = new HashMap<String, String>();
        
        Element rootElement = document.getDocumentElement();
        
        NodeList documentElements = rootElement.getElementsByTagName("signatures");
        Element signatures = (Element) documentElements.item(0);
        
        NodeList signaturesElements = signatures.getElementsByTagName("signature");
        for (int i = 0; i < signaturesElements.getLength(); i++) {
            Element signature = (Element) signaturesElements.item(i);

            NodeList signatureNameElements = signature.getElementsByTagName("name");
            NodeList signatureDigestElements = signature.getElementsByTagName("digest");

            Element nameElement = (Element) signatureNameElements.item(0);
            Element digestElement = (Element) signatureDigestElements.item(0);
            
            String name = nameElement.getTextContent();
            String digest = digestElement.getTextContent();
            
            extractedSignatures.put(name, digest);
        }
        
        return extractedSignatures;
    }
}
