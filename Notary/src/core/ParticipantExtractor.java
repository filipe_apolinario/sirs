package core;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ParticipantExtractor {
    public static List<String> extractParticipants(Document xmlDocument) throws RuntimeException {
        Element rootElement = xmlDocument.getDocumentElement();
        
        NodeList xmlDocumentElements = rootElement.getElementsByTagName("document");
        Element document = (Element) xmlDocumentElements.item(0);
        
        NodeList documentElements = document.getElementsByTagName("participants");
        Element participants = (Element) documentElements.item(0);

        NodeList participantsElements = participants.getElementsByTagName("name");
        List<String> extractedParticipants = new ArrayList<String>(participantsElements.getLength());
        for (int i = 0; i < participantsElements.getLength(); i++) {
            Element nameElement = (Element) participantsElements.item(i);
            
            String name = nameElement.getTextContent();
            
            extractedParticipants.add(name);
        }
        
        return extractedParticipants;
    }
}
