package core;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.util.List;
import java.util.Map;

import core.security.SignatureHandler;
import core.security.ws.MessageParser;

public class SignatureChecker {

    public static boolean checkSignature(String text,
                                         List<String> participants,
                                         Map<String, PublicKey> keys,
                                         Map<String, String> signatures) {
        byte[] textBytes = text.getBytes();

        //verify if all the participants' signatures are valid for the given text
        for (String p : participants) {

            PublicKey participantKey = keys.get(p);
            byte[] signature = MessageParser.fromXML(signatures.get(p));

            if (!hasParticipantSignedText(textBytes, p, participantKey, signature)) {
                return false;
            }
        }
        return true;
    }

    private static boolean hasParticipantSignedText(byte[] text,
                                                    String p,
                                                    PublicKey key,
                                                    byte[] digitalSignature) {

        try {
            if (!SignatureHandler.verifyDigitalSignature(text, digitalSignature, key)) {
                return false;
            }
        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            return false;
        }
        return true;
    }
}
