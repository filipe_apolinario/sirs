package core.security.ws;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

/**
 * The Class MessageParser is used to manipulate messages so that they can be used in
 * different utilities.
 */
public final class MessageParser {

    /**
     * Instantiates a new message parser.
     */
    private MessageParser() {

    }

    /**
     * Transforms a message into a byte array.
     * 
     * @param message the message
     * @return the byte[]
     */
    public static byte[] toByteArray(String message) {
        return message.getBytes();
    }

    /**
     * From byte array.
     * 
     * @param byteArray the byte array
     * @return the string
     */
    public static String fromByteArray(byte[] byteArray) {
        return new String(byteArray);
    }

    /**
     * Parses the given byte array to a base 64 binary so that it can be sent to a xml
     * file.
     * 
     * @param byteArray the byte array to be parsed
     * @return Base 64 Binary representation of the given message
     */
    public static String toXML(byte[] byteArray) {
        return printBase64Binary(byteArray);
    }

    /**
     * Parses the given base 64 binary to a byte array.
     * 
     * @param base64 the base 64 binary to be parsed
     * @return the byte array taken from the given base 64 binary
     */
    public static byte[] fromXML(String base64) {
        return parseBase64Binary(base64);
    }
}
