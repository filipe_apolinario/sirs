package core.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class KeyManager {

    private static String notaryPrivateKeyPath = "chave/" + "Public.key";
    private static String notaryPublicKeyPath = "chave/" + "Private.key";

    private static void writeFile(String path, byte[] content) throws FileNotFoundException,
            IOException {
        FileOutputStream fos = new FileOutputStream(path);
        fos.write(content);
        fos.close();
    }

    public static void writeNotaryRSAKeyPair(KeyPair keyPair) throws Exception {
        //TODO Remove System.out
        // generate RSA key pair


        System.out.println("Public key info:");
        System.out.println("algorithm: " + keyPair.getPublic().getAlgorithm());
        System.out.println("format: " + keyPair.getPublic().getFormat());

        System.out.println("---");

        System.out.println("Private key info:");
        System.out.println("algorithm: " + keyPair.getPrivate().getAlgorithm());
        System.out.println("format: " + keyPair.getPrivate().getFormat());

        System.out.println("Writing private key to '" + notaryPrivateKeyPath + "' ...");
        byte[] privEncoded = keyPair.getPrivate().getEncoded();
        writeFile(notaryPrivateKeyPath, privEncoded);

        System.out.println("Writing public key to " + notaryPublicKeyPath + " ...");
        byte[] pubEncoded = keyPair.getPublic().getEncoded();
        writeFile(notaryPublicKeyPath, pubEncoded);


    }

    public static KeyPair generateRSAKeyPair(int keySize) throws NoSuchAlgorithmException,
            InvalidAlgorithmParameterException {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keySize);
        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    private static byte[] readFile(String path) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] content = new byte[fis.available()];
        fis.read(content);
        fis.close();
        return content;
    }

    public static PrivateKey readNotaryRSAPrivateKey() throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            FileNotFoundException,
            IOException {
        byte[] privEncoded = readFile(notaryPrivateKeyPath);
        PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(privEncoded);
        KeyFactory keyFacPriv = KeyFactory.getInstance("RSA");
        final PrivateKey key = keyFacPriv.generatePrivate(privSpec);
        return key;
    }

    public static PublicKey readNotaryRSAPublicKey() throws FileNotFoundException,
            IOException,
            NoSuchAlgorithmException,
            InvalidKeySpecException {
        byte[] pubEncoded = readFile(notaryPublicKeyPath);
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pubEncoded);
        KeyFactory keyFacPub = KeyFactory.getInstance("RSA");
        final PublicKey key = keyFacPub.generatePublic(pubSpec);
        return key;
    }

    public static PublicKey byteToPublicKey(byte[] key) throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            FileNotFoundException,
            IOException {
        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(key);
        KeyFactory keyFacPub = KeyFactory.getInstance("RSA");
        final PublicKey publicKey = keyFacPub.generatePublic(pubSpec);
        return publicKey;
    }

}
