package service;

import static javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import presentation.ws.uddi.UDDINaming;
import ws.*;
public class ValidateDocumentService {

    private final String document;
    private String result;

    public ValidateDocumentService(String document) {
        this.document = document;
    }

    public void execute() throws RuntimeException {
        NotaryImpl port = null;
        try {
            String uddiURL = "http://localhost:8081";
            String name = "Notary";

            System.out.printf("Contacting UDDI at %s%n", uddiURL);
            UDDINaming uddiNaming = new UDDINaming(uddiURL);

            System.out.printf("Looking for '%s'%n", name);
            String endpointAddress = uddiNaming.lookup(name);

            if (endpointAddress == null) {
                System.out.println("Not found!");
                return;
            } else {
                System.out.printf("Found %s%n", endpointAddress);
            }

            System.out.println("Creating stub ...");
            NotaryImplService service = new NotaryImplService();
            port = service.getNotaryImplPort();

            System.out.println("Setting endpoint address ...");
            BindingProvider bindingProvider = (BindingProvider) port;
            Map<String, Object> requestContext = bindingProvider.getRequestContext();
            requestContext.put(ENDPOINT_ADDRESS_PROPERTY, endpointAddress);

            System.out.println("Remote call ...");

            String certifiedDocument = port.signDocument(document);

            result = certifiedDocument;


        } catch (Exception e) {
            //FIXME
            throw new RuntimeException("Could not connect to TSA");
        }

        //TODO Validation of the input, more specifically, the signature of the notary
    }

    public String getResult() {
        return result;
    }
}
