package presentation.cli;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import service.ValidateDocumentService;
import exception.BadConnectionException;
import exception.InvalidSignatureValidationException;


public class ClientMain {

    private static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static void main(String[] args) {
        System.out.print("Enter the document name: ");
        //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String documentName = null;

        //try {
        //    documentName = br.readLine();
        //    br.close();
        //} catch (IOException ioe) {
        //    System.out.println("IO error trying to read your name!");
        //    System.exit(1);
        //}
        //
        //if (documentName == null) {
        //    System.out.println("Ocorreu um erro");
        //    return;
        //}
        //
        String document = null;
        documentName = "document1.xml";
        try {
            document = readFile(documentName, Charset.defaultCharset());
        } catch (IOException e) {
            System.out.println("Ocorreu um erro ao tentar abrir o ficheiro");
        }

        if (document == null) {
            System.out.println("Ocorreu um erro ao ler o conteudo do ficheiro");
            return;
        }
        //document = "olá";
        ValidateDocumentService s = null;

        try {
            System.out.println("documento: \n\n" + document);
            s = new ValidateDocumentService(document);
        } catch (Exception e) {
            System.out.println("something went wrong");
        }
        try {
            s.execute();
            System.out.println("signed document:" + s.getResult());
        } catch (BadConnectionException e) {
            e.printStackTrace();
        } catch (InvalidSignatureValidationException e) {
            e.printStackTrace();
        }

    }
}
