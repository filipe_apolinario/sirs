package presentation.ws.handler;

import java.util.Iterator;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;

/**
 * The Class SoapUtilities.
 */
public final class SoapUtilities {

    /**
     * Instantiates a new soap utilities.
     */
    private SoapUtilities() {
    }

    /**
     * Gets the function name of a Cheque Refeicao request.
     * 
     * @param se the se
     * @return the function name
     * @throws SOAPException the SOAP exception
     */
    public static String getFunctionName(SOAPEnvelope se) throws SOAPException {
        SOAPBody sb = se.getBody();
        Iterator<?> it1 = sb.getChildElements();
        while (it1.hasNext()) {
            SOAPElement element1 = (SOAPElement) it1.next();
            return element1.getLocalName().toString();
        }
        return null;
    }


    /**
     * Gets the sender.
     * 
     * @param se the se
     * @return the sender
     * @throws SOAPException the SOAP exception
     */
    public static String getSender(SOAPEnvelope se) throws SOAPException {
        SOAPBody sb = se.getBody();

        Iterator<?> it = sb.getChildElements();

        if (it.hasNext()) {
            SOAPElement soapElement = (SOAPElement) it.next();
            Iterator<?> elementIter = soapElement.getChildElements();
            if (elementIter.hasNext()) {
                SOAPElement senderElement = (SOAPElement) elementIter.next();
                String name = senderElement.getLocalName().toString();
                if ("titular".equals(name) || "beneficiario".equals(name))
                    return senderElement.getValue();
            }
        }
        return null;
    }
}
